const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const RemovePlugin = require("remove-files-webpack-plugin");

module.exports = {
  watch: true,
  entry: {
    // app: ["./src/app.scss", "./src/app.js"],
    // hippo: ["./src/yoyo.scss"]
    app: "./src/app.scss",
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css",
    }),

    new RemovePlugin({
      after: {
        include: ["./dist/app.js"],
      },
    }),
  ],

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
    ],
  },
};